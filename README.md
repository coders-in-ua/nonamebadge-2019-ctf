# NoNameBadge 2019 CTF

Collaborative NoNameBadge CTF Write-Up.

Suggest your write-ups as a merge request. Keep this filename convention: **challenge_dir/your_nickname_or_email.md**

[NoNameBadge Instruction page](https://www.nonamecon.org/badge)

[Official Leaderboard](https://spynet.techmaker.ua/scores)

# Contents

## Rooting

[Take a closer look at NoNameBadge Trailer](https://www.youtube.com/watch?v=chfoAWevHMs)

## Mr Bean Walker

 - [Anvol](Mr Bean Walker/anvol.md)
 - [VVS](Mr Bean Walker/blackvs.md)

## BruteSearcher

 - [Anvol](BruteSearcher/anvol.md)
 - [VVS](BruteSearcher/blackvs.md)

## NoNameCon SpyNet

 - [Anvol](NoNameCon SpyNet/anvol.md)
 - [VVS](NoNameCon SpyNet/blackvs.md)

## Ployka PWNer

 - [Anvol](Ployka PWNer/anvol.md)
 - [VVS](Ployka PWNer/blackvs.md)

## Binary Hero

 - [Anvol](Binary Hero/anvol.md)
 - [VVS](Binary Hero/blackvs.md)

## Side Blennel

 - [Anvol](Side Blennel/anvol.md)
 - [VVS](Side Blennel/blackvs.md)
